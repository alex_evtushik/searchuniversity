package my.test.searchuniversity.listeners;

/**
 * Created by alex on 6/12/17.
 */

public interface OnItemClickListener {
    void onClick(int pos);
}
