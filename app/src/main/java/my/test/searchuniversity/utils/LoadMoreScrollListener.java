package my.test.searchuniversity.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by alex on 6/12/17.
 */

public abstract class LoadMoreScrollListener extends RecyclerView.OnScrollListener {

    protected boolean isLoading, isLastCallFailed, isAllLoaded;
    protected int offset = 0;

    public abstract void onLoad(int offset);

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

        int totalItemCount = layoutManager.getItemCount();
        int lastVisibleItem = layoutManager.findLastVisibleItemPosition();
        boolean offsetReached = totalItemCount - lastVisibleItem <= 3;

        if (isAllLoaded) {
            return;
        }

        if (totalItemCount > 5 && offsetReached && !isLoading && !isLastCallFailed) {
            isLoading = true;
            onLoad(offset);
        }
    }

    public void notifyFail() {
        isLoading = false;
        isLastCallFailed = true;
    }

    public void notifyFinish(int lastBunchSize) {
        offset += lastBunchSize;
        isLoading = false;
        isAllLoaded = lastBunchSize == 0;
        isLastCallFailed = false;
    }

    public void notifyFinish(boolean allLoaded) {
        isLoading = false;
        isAllLoaded = allLoaded;
        isLastCallFailed = false;
    }

    public void reset() {
        offset = 0;
        isLoading = isLastCallFailed = isAllLoaded = false;
    }
}
