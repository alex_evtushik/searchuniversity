package my.test.searchuniversity.utils;

/**
 * Created by alex on 6/8/17.
 */

public class Constants {

    public static final String KEY_SEARCH_MODE = "search_mode";
    public static final String KEY_COUNTRY_ID = "country_id";
    public static final String KEY_CITY_ID = "city_id";
    public static final String KEY_UNIVERSITY_INFO = "university_info";

    private Constants() {
    }
}
