package my.test.searchuniversity.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import my.test.searchuniversity.R;
import my.test.searchuniversity.model.SearchSuggestion;
import my.test.searchuniversity.model.entities.City;
import my.test.searchuniversity.model.entities.Country;
import my.test.searchuniversity.model.entities.University;
import my.test.searchuniversity.network.RestClient;
import my.test.searchuniversity.ui.activities.MainActivity;
import my.test.searchuniversity.ui.adapters.SuggestionsAdapter;
import my.test.searchuniversity.ui.widgets.ProgressView;
import my.test.searchuniversity.utils.Constants;
import my.test.searchuniversity.utils.LoadMoreScrollListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alex on 6/12/17.
 */

public class SearchSuggestionsFragment extends Fragment {

    public static final String TAG = SearchSuggestionsFragment.class.getSimpleName();

    public enum Mode {
        COUNTRY, CITY, UNIVERSITY

    }

    @BindView(R.id.progress_view)
    protected ProgressView progressView;

    @BindView(R.id.rv_suggestions)
    protected RecyclerView rvSuggestions;

    @BindView(R.id.tv_empty_list)
    protected TextView tvEmptyList;

    private Mode mode = Mode.COUNTRY;
    private OnSuggestionSelected onSuggestionSelected;
    private SuggestionsAdapter adapter;
    private int countryId;
    private int cityId;

    private LoadMoreScrollListener loadMoreScrollListener = new LoadMoreScrollListener() {
        @Override
        public void onLoad(int offset) {
            if (offset > 0) {
                switch (mode) {
                    case COUNTRY:
                        searchMoreCountries(offset);
                        break;
                    case CITY:
                        searchMoreCities(offset);
                        break;
                    case UNIVERSITY:
                        searchMoreUniversities(offset);
                        break;
                }
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_suggestions, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MainActivity activity = (MainActivity) getActivity();
        onSuggestionSelected = activity;
        mode = Mode.values()[getArguments().getInt(Constants.KEY_SEARCH_MODE)];
        countryId = getArguments().getInt(Constants.KEY_COUNTRY_ID);
        cityId = getArguments().getInt(Constants.KEY_CITY_ID);

        adapter = new SuggestionsAdapter(activity, pos -> onSuggestionSelected.onSelected(adapter.getItem(pos)));
        rvSuggestions.setLayoutManager(new LinearLayoutManager(activity));
        rvSuggestions.setAdapter(adapter);
        rvSuggestions.setItemAnimator(new DefaultItemAnimator());
        rvSuggestions.addOnScrollListener(loadMoreScrollListener);

        search();
    }

    private void search() {
        loadMoreScrollListener.reset();

        switch (mode) {
            case COUNTRY:
                searchCountries();
                break;
            case CITY:
                searchCities();
                break;
            case UNIVERSITY:
                searchUniversities();
                break;
        }
    }

    private void searchCountries() {
        RestClient.getCountries(0, new Callback<Country>() {
            @Override
            public void onResponse(Call<Country> call, Response<Country> response) {
                if (isVisible()) {
                    if (response.isSuccessful()) {
                        List<SearchSuggestion> data = new ArrayList<>(response.body().getItems());
                        adapter.setData(data);
                        onSuccessRequest(data.size(), response.body().getCount());
                    } else {
                        onFailRequest(false);
                    }

                    progressView.hide();
                }
            }

            @Override
            public void onFailure(Call<Country> call, Throwable t) {
                if (isVisible()) {
                    progressView.hide();
                    Toast.makeText(getContext(), R.string.error_something_went_wrong, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void searchMoreCountries(int offset) {
        RestClient.getCountries(offset, new Callback<Country>() {
            @Override
            public void onResponse(Call<Country> call, Response<Country> response) {
                if (isVisible()) {
                    if (response.isSuccessful()) {
                        List<SearchSuggestion> data = new ArrayList<>(response.body().getItems());
                        adapter.addData(data);
                        onSuccessRequest(data.size(), response.body().getCount());
                    } else {
                        loadMoreScrollListener.notifyFail();
                        adapter.removeFooter();
                        Toast.makeText(getContext(), R.string.error_something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Country> call, Throwable t) {
                if (isVisible()) {
                    loadMoreScrollListener.notifyFail();
                    adapter.removeFooter();
                    Toast.makeText(getContext(), R.string.error_something_went_wrong, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void searchCities() {
        RestClient.getCities(countryId, 0, new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                if (isVisible()) {
                    if (response.isSuccessful()) {
                        List<SearchSuggestion> data = new ArrayList<>(response.body().getItems());
                        adapter.setData(data);
                        onSuccessRequest(data.size(), response.body().getCount());
                    } else {
                        onFailRequest(false);
                    }

                    progressView.hide();
                }
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                if (isVisible()) {
                    progressView.hide();
                    onFailRequest(false);
                }
            }
        });
    }

    private void searchMoreCities(int offset) {
        RestClient.getCities(countryId, offset, new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                if (response.isSuccessful()) {
                    List<SearchSuggestion> data = new ArrayList<>(response.body().getItems());
                    adapter.addData(data);
                    onSuccessRequest(data.size(), response.body().getCount());
                } else {
                    onFailRequest(true);
                }
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                if (isVisible()) {
                    onFailRequest(true);
                }
            }
        });
    }

    private void searchUniversities() {
        RestClient.getUniversities(countryId, cityId, 0, new Callback<University>() {
            @Override
            public void onResponse(Call<University> call, Response<University> response) {
                if (isVisible()) {
                    if (response.isSuccessful()) {
                        List<SearchSuggestion> data = new ArrayList<>(response.body().getItems());
                        adapter.setData(data);
                        onSuccessRequest(data.size(), response.body().getCount());
                    } else {
                        onFailRequest(false);
                    }

                    progressView.hide();
                }
            }

            @Override
            public void onFailure(Call<University> call, Throwable t) {
                if (isVisible()) {
                    progressView.hide();
                    onFailRequest(false);
                }
            }
        });
    }

    private void searchMoreUniversities(int offset) {
        RestClient.getUniversities(countryId, cityId, offset, new Callback<University>() {
            @Override
            public void onResponse(Call<University> call, Response<University> response) {
                if (isVisible()) {
                    if (response.isSuccessful()) {
                        List<SearchSuggestion> data = new ArrayList<>(response.body().getItems());
                        adapter.addData(data);
                        onSuccessRequest(data.size(), response.body().getCount());
                    } else {
                        onFailRequest(true);
                    }

                    progressView.hide();
                }
            }

            @Override
            public void onFailure(Call<University> call, Throwable t) {
                if (isVisible()) {
                    onFailRequest(true);

                }
            }
        });
    }

    private void onSuccessRequest(int dataSize, int responseItemCount) {
        tvEmptyList.setVisibility(adapter.isEmpty() ? View.VISIBLE : View.GONE);

        if (adapter.getItemCount() > responseItemCount) {
            loadMoreScrollListener.notifyFinish(true);
            adapter.removeFooter();
        } else {
            loadMoreScrollListener.notifyFinish(dataSize);
        }
    }

    private void onFailRequest(boolean isLoadMore) {
        Toast.makeText(getContext(), R.string.error_something_went_wrong, Toast.LENGTH_SHORT).show();
        loadMoreScrollListener.notifyFail();
        if (isLoadMore) {
            adapter.removeFooter();
        }
    }

    public interface OnSuggestionSelected {
        void onSelected(SearchSuggestion suggestion);
    }

    public static class Builder {
        private Mode mode;
        private int countryId;
        private int cityId;

        public Builder(Mode mode) {
            this.mode = mode;
        }

        public Builder setCountryId(int countryId) {
            this.countryId = countryId;
            return this;

        }

        public Builder setCityId(int cityId) {
            this.cityId = cityId;
            return this;

        }

        public SearchSuggestionsFragment newInstance() {
            Bundle args = new Bundle();
            args.putInt(Constants.KEY_SEARCH_MODE, mode.ordinal());
            args.putInt(Constants.KEY_COUNTRY_ID, countryId);
            args.putInt(Constants.KEY_CITY_ID, cityId);
            SearchSuggestionsFragment fragment = new SearchSuggestionsFragment();
            fragment.setArguments(args);
            return fragment;
        }

    }
}
