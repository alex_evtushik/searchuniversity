package my.test.searchuniversity.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import my.test.searchuniversity.R;
import my.test.searchuniversity.model.entities.UniversityInfo;
import my.test.searchuniversity.utils.Constants;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    @BindView(R.id.tv_address)
    protected TextView tvAddress;

    private UniversityInfo universityInfo;

    public static void start(Context context, UniversityInfo data) {
        Intent starter = new Intent(context, MapsActivity.class);
        starter.putExtra(Constants.KEY_UNIVERSITY_INFO, data);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        universityInfo = getIntent().getParcelableExtra(Constants.KEY_UNIVERSITY_INFO);
        tvAddress.setText(universityInfo.getAddress());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng point = new LatLng(universityInfo.getLat(), universityInfo.getLng());
        googleMap.addMarker(new MarkerOptions().position(point).title(universityInfo.getTitle()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 16.0f));
    }

    @OnClick(R.id.btn_directions)
    protected void showDirections() {
        Uri intentUri = Uri.parse("google.navigation:q=" + universityInfo.getAddress());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, intentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }
}
