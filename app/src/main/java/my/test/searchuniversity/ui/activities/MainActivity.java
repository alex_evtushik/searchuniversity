package my.test.searchuniversity.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import my.test.searchuniversity.R;
import my.test.searchuniversity.model.entities.City;
import my.test.searchuniversity.model.entities.Country;
import my.test.searchuniversity.model.entities.University;
import my.test.searchuniversity.model.entities.UniversityInfo;
import my.test.searchuniversity.model.SearchSuggestion;
import my.test.searchuniversity.ui.fragments.SearchSuggestionsFragment;
import my.test.searchuniversity.ui.widgets.ProgressView;

public class MainActivity extends AppCompatActivity implements SearchSuggestionsFragment.OnSuggestionSelected, GoogleApiClient.ConnectionCallbacks {

    @BindView(R.id.progress_view)
    protected ProgressView progressView;

    @BindView(R.id.tv_country)
    protected TextView tvCountry;

    @BindView(R.id.tv_city)
    protected TextView tvCity;

    @BindView(R.id.tv_university)
    protected TextView tvUniversity;

    private Country country;
    private City city;
    private University university;
    private GoogleApiClient googleApiClient;
    private boolean isPlayServicesEnabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onClickHome();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            onClickHome();
        }
        super.onBackPressed();
    }

    @OnClick(R.id.tv_country)
    protected void onSelectCountry() {
        Fragment fragment = new SearchSuggestionsFragment.Builder(SearchSuggestionsFragment.Mode.COUNTRY)
                .newInstance();
        showFragment(fragment, SearchSuggestionsFragment.TAG);
    }

    @OnClick(R.id.tv_city)
    protected void onSelectCity() {
        if (country != null) {
            Fragment fragment = new SearchSuggestionsFragment.Builder(SearchSuggestionsFragment.Mode.CITY)
                    .setCountryId(country.getId())
                    .newInstance();
            showFragment(fragment, SearchSuggestionsFragment.TAG);
        } else {
            Toast.makeText(this, R.string.error_empty_country_field, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.tv_university)
    protected void onSelectUniversity() {
        if (country != null && city != null) {
            Fragment fragment = new SearchSuggestionsFragment.Builder(SearchSuggestionsFragment.Mode.UNIVERSITY)
                    .setCountryId(country.getId())
                    .setCityId(city.getId())
                    .newInstance();
            showFragment(fragment, SearchSuggestionsFragment.TAG);
        } else {
            Toast.makeText(this, R.string.error_empty_city_field, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_search)
    protected void onSearch() {
        if (university != null) {
            if (isPlayServicesEnabled) {
                progressView.show();
                processQuery(university.getTitle());
            } else {
                Toast.makeText(this, R.string.error_gps_not_available, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.error_empty_university_field, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSelected(SearchSuggestion suggestion) {
        if (suggestion instanceof Country) {
            country = (Country) suggestion;
            tvCountry.setText(country.getTitle());
            city = null;
            tvCity.setText("");
            university = null;
            tvUniversity.setText("");
        } else if (suggestion instanceof City) {
            city = (City) suggestion;
            tvCity.setText(city.getTitle());
            university = null;
            tvUniversity.setText("");
        } else if (suggestion instanceof University) {
            university = (University) suggestion;
            tvUniversity.setText(university.getTitle());
            onSearch();
        }

        onClickHome();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        isPlayServicesEnabled = true;
    }

    @Override
    public void onConnectionSuspended(int i) {
        isPlayServicesEnabled = true;
    }

    private void showFragment(Fragment fragment, String tag) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.container, fragment)
                .addToBackStack(tag)
                .commitAllowingStateLoss();
    }

    private void onClickHome() {
        getSupportFragmentManager().popBackStack();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    public void processQuery(String query) {
        AutocompleteFilter mAutocompleteFilter = new AutocompleteFilter.Builder().build();
        PendingResult<AutocompletePredictionBuffer> result = Places.GeoDataApi.getAutocompletePredictions(googleApiClient, query, null, mAutocompleteFilter);

        result.setResultCallback(autocompletePredictions -> {
            if (autocompletePredictions.getCount() > 0) {
                processPlace(autocompletePredictions.get(0).getPlaceId());
            } else {
                progressView.hide();
                Toast.makeText(MainActivity.this, R.string.error_university_not_found, Toast.LENGTH_SHORT).show();
            }
            autocompletePredictions.release();
        });
    }

    private void processPlace(String placeId) {
        Places.GeoDataApi.getPlaceById(googleApiClient, placeId).setResultCallback(places -> {
            progressView.hide();

            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                Place place = places.get(0);
                UniversityInfo universityInfo = new UniversityInfo(place.getName().toString(),
                        place.getAddress().toString(), place.getLatLng().latitude, place.getLatLng().longitude);

                MapsActivity.start(MainActivity.this, universityInfo);
            } else {
                Toast.makeText(MainActivity.this, R.string.error_university_not_found, Toast.LENGTH_SHORT).show();
            }
            places.release();
        });
    }
}
