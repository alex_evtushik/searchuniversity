package my.test.searchuniversity.ui.widgets;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import my.test.searchuniversity.R;

/**
 * Created by alex on 6/12/17.
 */

public class ProgressView extends FrameLayout {

    private Context context;
    private boolean hiding;
    private boolean showing;
    private boolean needToHide;

    public ProgressView(Context context) {
        super(context);
        if (!isInEditMode()) {
            init(context);
        }
    }

    public ProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init(context);
        }
    }

    public ProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            init(context);
        }
    }

    public void hide() {
        if (showing) {
            needToHide = true;
        } else if (getVisibility() == VISIBLE && !hiding) {
            hiding = true;

            Animation animation = AnimationUtils.loadAnimation(context, R.anim.fade_out);
            animation.setAnimationListener(new LayerEnablingAnimationListener(this) {
                @Override
                public void animationEnd() {
                    setVisibility(GONE);
                    hiding = false;
                }
            });
            startAnimation(animation);
        }
    }

    public void show() {
        if (getVisibility() == GONE && !showing) {
            showing = true;

            setVisibility(VISIBLE);
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.fade_in);
            animation.setAnimationListener(new LayerEnablingAnimationListener(this) {
                @Override
                public void animationEnd() {
                    showing = false;
                    if (needToHide) {
                        hide();
                    }
                }
            });
            startAnimation(animation);
        }
    }

    private void init(Context context) {
        this.context = context;
        LayoutInflater.from(context).inflate(R.layout.view_progress, this, true);
        setBackgroundColor(Color.WHITE);
    }

    public class LayerEnablingAnimationListener implements Animation.AnimationListener {

        private View[] views = new View[0];
        private int[] viewLayerTypes = new int[0];

        public LayerEnablingAnimationListener(View... views) {
            this.views = views;
            viewLayerTypes = new int[views.length];
        }

        @Override
        public void onAnimationStart(Animation animation) {
            for (int i = 0; i < views.length; i++) {
                viewLayerTypes[i] = views[i].getLayerType();
                views[i].setLayerType(View.LAYER_TYPE_HARDWARE, null);
            }
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            recoverLayerTypes();
            animationEnd();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }

        public void animationEnd() {

        }

        private void recoverLayerTypes() {
            for (int i = 0; i < views.length; i++) {
                views[i].setLayerType(viewLayerTypes[i], null);
            }
        }
    }
}
