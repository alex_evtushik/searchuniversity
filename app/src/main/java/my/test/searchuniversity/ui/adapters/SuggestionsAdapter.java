package my.test.searchuniversity.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import my.test.searchuniversity.R;
import my.test.searchuniversity.listeners.OnItemClickListener;
import my.test.searchuniversity.model.SearchSuggestion;
import my.test.searchuniversity.ui.widgets.ProgressView;

/**
 * Created by alex on 6/12/17.
 */

public class SuggestionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_VIEW_TYPE = 0;
    private static final int FOOTER_VIEW_TYPE = 1;

    private List<SearchSuggestion> data = new ArrayList<>();
    private boolean hasFooter = true;
    private Context context;
    private OnItemClickListener onItemClickListener;

    public SuggestionsAdapter(Context context, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == FOOTER_VIEW_TYPE) {
            return new FooterViewHolder(LayoutInflater.from(context).inflate(R.layout.item_footer_progress, parent, false));
        }

        View view = LayoutInflater.from(context).inflate(R.layout.item_search_suggestion, parent, false);
        return new ItemViewHolder(view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == ITEM_VIEW_TYPE) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.title.setText(getItem(position).getTitle());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1 && hasFooter) {
            return FOOTER_VIEW_TYPE;
        }

        return ITEM_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        return data.size() + getFooterCount();
    }

    public void setData(List<SearchSuggestion> data) {
        this.data = data;
        this.notifyDataSetChanged();
    }

    public void addData(List<SearchSuggestion> data) {
        int oldCount = this.data.size();
        this.data.addAll(data);
        this.notifyItemRangeInserted(oldCount, data.size());
    }

    public boolean isEmpty() {
        return data.isEmpty();
    }

    public SearchSuggestion getItem(int pos) {
        return data.get(pos);
    }

    public int getFooterCount() {
        return hasFooter ? 1 : 0;
    }

    public void removeFooter() {
        this.hasFooter = false;
        this.notifyItemRemoved(getItemCount());
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        public TextView title;

        public ItemViewHolder(View itemView, OnItemClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onClick(getAdapterPosition());
                }
            });

        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progress_view)
        public ProgressView progressView;

        FooterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void showProgress() {
            progressView.show();
        }

        private void hideProgress() {
            progressView.hide();
        }
    }
}
