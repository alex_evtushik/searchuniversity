package my.test.searchuniversity.network;

import my.test.searchuniversity.model.entities.City;
import my.test.searchuniversity.model.entities.Country;
import my.test.searchuniversity.model.entities.University;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by alex on 6/12/17.
 */

public interface SearchService {

    @GET("database.getUniversities?v=5.65")
    Call<University> getUniversities(@Query("country_id") int countryId,
                                                     @Query("city_id") int cityId,
                                                     @Query("offset") int offset,
                                                     @Query("count") int count);

    @GET("database.getCities?v=5.65")
    Call<City> getCities(@Query("country_id") int countryId,
                                         @Query("offset") int offset,
                                         @Query("count") int count);

    @GET("database.getCountries?need_all=true&v=5.65")
    Call<Country> getCountries(@Query("offset") int offset, @Query("count") int count);


}
