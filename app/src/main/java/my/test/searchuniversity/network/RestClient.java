package my.test.searchuniversity.network;

import my.test.searchuniversity.model.entities.City;
import my.test.searchuniversity.model.entities.Country;
import my.test.searchuniversity.model.entities.University;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alex on 6/12/17.
 */

public class RestClient {

    private static final int DEFAULT_ITEMS_COUNT = 25;

    private static final String API_BASE_URL = "https://api.vk.com/method/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build();

    private static SearchService searchService = retrofit.create(SearchService.class);

    public static void getCountries(int offset, Callback<Country> callback) {
        searchService.getCountries(offset, DEFAULT_ITEMS_COUNT).enqueue(callback);
    }

    public static void getCities(int countryId, int offset, Callback<City> callback) {
        searchService.getCities(countryId, offset, DEFAULT_ITEMS_COUNT).enqueue(callback);
    }

    public static void getUniversities(int countryId, int cityId, int offset, Callback<University> callback) {
        searchService.getUniversities(countryId, cityId, offset, DEFAULT_ITEMS_COUNT).enqueue(callback);
    }
}
