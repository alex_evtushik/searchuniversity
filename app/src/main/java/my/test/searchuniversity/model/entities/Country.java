package my.test.searchuniversity.model.entities;

import java.util.ArrayList;
import java.util.List;

import my.test.searchuniversity.model.SearchSuggestion;

/**
 * Created by alex on 6/12/17.
 */

public class Country implements SearchSuggestion {

    private Response response = new Response();
    public int id;
    public String title;

    public Country(int id, String title) {
        this.id = id;
        this.title = title;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Country{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }

    public int getCount() {
        return response.count;
    }

    public List<Country> getItems() {
        return response.items;
    }

    class Response {
        public int count;
        public List<Country> items = new ArrayList<>();
    }
}
