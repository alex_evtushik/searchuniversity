package my.test.searchuniversity.model.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alex on 6/12/17.
 */

public class UniversityInfo implements Parcelable {

    private String title;
    private String address;
    private double lat;
    private double lng;

    public UniversityInfo(String title, String address, double lat, double lng) {
        this.title = title;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
    }

    protected UniversityInfo(Parcel in) {
        title = in.readString();
        address = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
    }

    public String getTitle() {
        return title;
    }

    public String getAddress() {
        return address;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(address);
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
    }

    public static final Creator<UniversityInfo> CREATOR = new Creator<UniversityInfo>() {
        @Override
        public UniversityInfo createFromParcel(Parcel in) {
            return new UniversityInfo(in);
        }

        @Override
        public UniversityInfo[] newArray(int size) {
            return new UniversityInfo[size];
        }
    };
}
