package my.test.searchuniversity.model;

/**
 * Created by alex on 6/12/17.
 */

public interface SearchSuggestion {

    int getId();
    String getTitle();
}
