package my.test.searchuniversity.model.entities;

import java.util.ArrayList;
import java.util.List;

import my.test.searchuniversity.model.SearchSuggestion;

/**
 * Created by alex on 6/12/17.
 */

public class University implements SearchSuggestion {

    private Response response = new Response();
    private int id;
    private String title;

    public University(int id, String title) {
        this.id = id;
        this.title = title;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "University{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }

    public int getCount() {
        return response.count;
    }

    public List<University> getItems() {
        return response.items;
    }

    class Response {
        public int count;
        public List<University> items = new ArrayList<>();
    }
}
